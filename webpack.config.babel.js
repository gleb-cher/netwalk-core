const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const pkg = require('./package.json');

const libraryName = pkg.name;


export default (env, argv) => {
  return {
    entry: './src/index.js',
    devtool: argv.mode === 'development' ? 'source-map' : false,
    devServer: {
      contentBase: './dist',
      index: 'demo.html',
    },
    output: {
      path: path.resolve(__dirname, './dist'),
      filename: `${libraryName}.${argv.mode === 'production' ? 'min' : ''}.js`,
      libraryTarget: 'umd',
      library: 'netwalkCore',
      umdNamedDefine: true,
    },
    module: {
      rules: [
        {
          test: /\.js$/,
          exclude: /node_modules/,
          use: 'babel-loader',
        },
      ],
    },
    plugins: [
      new HtmlWebpackPlugin({
        filename: 'demo.html',
        title: libraryName,
      })],
  };
};
