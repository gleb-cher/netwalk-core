import Vector from 'victor';

export const directions = [
  { name: 'up', vector: new Vector(0, -1) },
  { name: 'right', vector: new Vector(1, 0) },
  { name: 'down', vector: new Vector(0, 1) },
  { name: 'left', vector: new Vector(-1, 0) },
];

export const oppositeConnections = {
  up: 'down',
  right: 'left',
  down: 'up',
  left: 'right',
};

export const nextConnections = {
  up: 'right',
  right: 'down',
  down: 'left',
  left: 'up',
};

// clockwise movement
export const connectionTypes = {
  crosshair: [
    ['up', 'right', 'down', 'left'],
  ],
  tee: [
    ['up', 'right', 'down'],
    ['right', 'down', 'left'],
    ['down', 'left', 'up'],
    ['left', 'up', 'right'],
  ],
  elbow: [
    ['up', 'right'],
    ['right', 'down'],
    ['down', 'left'],
    ['left', 'up'],
  ],
  straight: [
    ['up', 'down'],
    ['right', 'left'],
  ],
  half: [
    ['up'],
    ['right'],
    ['down'],
    ['left'],
  ],
};
