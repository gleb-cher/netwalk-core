import Vector from 'victor';
import _difference from 'lodash/difference';
import _uniqueId from 'lodash/uniqueId';

import { connectionTypes, nextConnections } from './constants';

export default class Cell {
  constructor({
    x,
    y,
    type = null,
    connections = [],
    connected = false,
  }) {
    if (x === undefined || y === undefined) throw new Error('x and y required');

    this.id = _uniqueId(`${y}-${x}--`);
    this.type = type;
    this.connections = connections;
    this.connected = connected || type === 'provider';
    this.actualized = type === 'provider';
    this.vector = Vector(x, y);
    this.randomized = false;
  }

  rotateNode() {
    if (this.connections.length === 0) return new Error('attempt to rotate cell with no connections');
    this.connections = this.connections.map(connection => nextConnections[connection]);
  }

  static getConnectionParamsByConnections(connections) {
    const connectionTypesNames = Object.keys(connectionTypes);
    for (const connectionTypeName of connectionTypesNames) {
      const connectionTemplates = connectionTypes[connectionTypeName];
      for (let i = 0; i < connectionTemplates.length; i++) {
        if (_difference(connectionTemplates[i], connections).length === 0) {
          return {
            position: i,
            type: connectionTypeName,
          };
        }
      }
    }
    return null;
  }
}
