# netwalk-core
Netwalk game (aka pipes, network) core.

## Installation
```
npm i -S netwalk-core
```

## Usage

### ES6 import
```javascript
import * as netwalkCore from 'netwalk-core';
const { Netwalk } = netwalkCore;

const params = {};
const instance = new Netwalk(params);
```
### Script tag in HTML
```html
<script src="netwalk-core.min.js"></script>
```
then in js
```javascript
const { Netwalk } = window.netwalkCore

const params = {}
const instance = new Netwalk(params)
```

By default, instance create matrix, then fill and randomize it.

You can change this behavior by passing params object with **fill** and **randomize** boolean fields.

## API
### Netwalk class
#### Init params
```javascript
const params = {
    template,
    rows,
    columns,
    matrix,

    fill,
    fillAnimationDelay,

    randomize,
    randomizeAnimationDelay,


    matrixChangeCallback,
    
    stopwatchChangeCallback,
    movesChangeCallback,
    
    startedChangeCallback,
    finishedChangeCallback,
    
    readyChangeCallback,
    
    matrixRandomizedChangeCallback,
    matrixFilledChangeCallback,
    matrixFillingChangeCallback,
    matrixRandomizingChangeCallback,
}
```
##### template (_Two dimensional array_)
- 0 is empty cell
- 1 is consumer or simple cell
- 'provider' is initial provider cell

The template must be resolvable, otherwise an error will be thrown.

Template examples:
 ```javascript
const P = 'provider'

const goodTemplate1 = [
      [1, 1, 1, 1, 1],
      [1, 1, 1, 1, 1],
      [1, 1, 1, 1, 1],
      [1, 1, 1, 1, 1]
  ]
      
const goodTemplate2 = [
      [1, 1, 1, 1, 0],
      [1, 1, P, 1, 0],
      [1, 1, 1, 1, 0],
      [1, 1, 1, 1, 0]
  ]

const badTemplate = [
    [1, 1, 1, 1, 0],
    [1, 1, 1, 1, 0],
    [1, 1, 1, 0, 0],
    [1, 1, 1, 0, 1] // error in last cell 
]
 ```
##### rows (_number_)


##### columns (_number_)


##### matrix (_Matrix with Cells_)

if you already have a Matrix instance.


##### fill (_boolean_)

Default = true
    
    
##### randomize (_boolean_)

Default = true


##### fillAnimationDelay (_number_)

Delay (in ms) before filling every cell.

Default = 0

##### randomizeAnimationDelay (_number_)

Delay (in ms) before randomizing every cell.

Default = 0

##### Callbacks
- matrixChangeCallback 
- stopwatchChangeCallback
- movesChangeCallback 
- startedChangeCallback 
- finishedChangeCallback, 
- readyChangeCallback
- matrixFillingChangeCallback
- matrixFilledChangeCallback
- matrixRandomizingChangeCallback
- matrixRandomizedChangeCallback

_Every callback will be called with the changed value (except matrixChangeCallback)._

#### Instance methods 
##### fillMatrix ()
_return Promise_

Creates provider (if it doesn't exist), consumers and nodes. 

##### randomizeMatrix ()
_return Promise_

Randomly changes rotate position of the node.

##### destroy ()
Stop stopwatch and another internal processes.

##### rotateNode (id)
Matrix consist of Cell class objects with IDs.

Example:
```javascript
instance.rotate(Cell.id)
```

#### Instance props
- matrix
- started
- finished
- ready
- moves
- stopwatch

### Auxiliary constants

```javascript
import {directions, oppositeConnections, nextConnections, connectionTypes} from 'netwalk-core' 
```
Can help you in building a view.

